RHQ_specFor_A2 = 
	[
	"RUS_Soldier_Sab",
	"RUS_Soldier_GL",
	"RUS_Soldier_Marksman",
	"RUS_Commander",
	"RUS_Soldier1",
	"RUS_Soldier2",
	"RUS_Soldier3",
	"RUS_Soldier_TL"
	];
	
RHQ_recon_A2 =
	[
	"FR_TL",
	"FR_Sykes",
	"FR_R",
	"FR_Rodriguez",
	"FR_OHara",
	"FR_Miles",
	"FR_Marksman",
	"FR_AR",
	"FR_GL",
	"FR_AC",
	"FR_Sapper",
	"FR_Corpsman",
	"FR_Cooper",
	"FR_Commander",
	"FR_Assault_R",
	"FR_Assault_GL",
	"USMC_SoldierS_Spotter",
	"USMC_SoldierS",
	"MQ9PredatorB",
	"CDF_Soldier_Spotter",
	"RU_Soldier_Spotter",
	"RUS_Soldier3",
	"Pchela1T",
	"GUE_Soldier_Scout"
	];
	
RHQ_FO_A2 =
	[
	"USMC_SoldierS_Spotter",
	"CDF_Soldier_Spotter",
	"RU_Soldier_Spotter",
	"Ins_Soldier_CO",
	"GUE_Soldier_Scout"
	];
	
RHQ_snipers_A2 = 
	[
	"USMC_SoldierS_Sniper",
	"USMC_SoldierS_SniperH",
	"USMC_SoldierM_Marksman",
	"FR_Marksman",
	"CDF_Soldier_Sniper",
	"CDF_Soldier_Marksman",
	"RU_Soldier_Marksman",
	"RU_Soldier_Sniper",
	"RU_Soldier_SniperH",
	"MVD_Soldier_Marksman",
	"MVD_Soldier_Sniper",
	"RUS_Soldier_Marksman",
	"Ins_Soldier_Sniper",
	"GUE_Soldier_Sniper"
	];
	
RHQ_ATinf_A2 = 
	[
	"USMC_Soldier_HAT",
	"USMC_Soldier_AT",
	"USMC_Soldier_LAT",
	"HMMWV_TOW",
	"CDF_Soldier_RPG",
	"RU_Soldier_HAT",
	"RU_Soldier_AT",
	"RU_Soldier_LAT",
	"MVD_Soldier_AT",
	"Ins_Soldier_AT",
	"GUE_Soldier_AT"
	];
	
RHQ_AAinf_A2 = 
	[
	"USMC_Soldier_AA",
	"HMMWV_Avenger",
	"CDF_Soldier_Strela",
	"Ural_ZU23_CDF",
	"RU_Soldier_AA",
	"2S6M_Tunguska",
	"Ins_Soldier_AA",
	"ZSU_INS",
	"Ural_ZU23_INS",
	"GUE_Soldier_AA",
	"Ural_ZU23_Gue"
	];
	
RHQ_Inf_A2 = 
	[
	"GUE_Commander",
	"GUE_Soldier_Scout",
	"GUE_Soldier_Sab",
	"GUE_Soldier_AA",
	"GUE_Soldier_AT",
	"GUE_Soldier_1",
	"GUE_Soldier_2",
	"GUE_Soldier_3",
	"GUE_Soldier_Pilot",
	"GUE_Soldier_Medic",
	"GUE_Soldier_MG",
	"GUE_Soldier_Sniper",
	"GUE_Soldier_GL",
	"GUE_Soldier_Crew",
	"GUE_Soldier_CO",
	"GUE_Soldier_AR",
	"Ins_Woodlander1",
	"Ins_Villager4",
	"Ins_Worker2",
	"Ins_Woodlander2",
	"Ins_Woodlander3",
	"Ins_Villager3",
	"Ins_Soldier_Sniper",
	"Ins_Soldier_Sapper",
	"Ins_Soldier_Sab",
	"Ins_Soldier_2",
	"Ins_Soldier_1",
	"Ins_Soldier_Pilot",
	"Ins_Soldier_CO",
	"Ins_Soldier_Medic",
	"Ins_Soldier_MG",
	"Ins_Bardak",
	"Ins_Soldier_GL",
	"Ins_Soldier_Crew",
	"Ins_Commander",
	"Ins_Lopotev",
	"Ins_Soldier_AR",
	"Ins_Soldier_AT",
	"Ins_Soldier_AA",
	"RUS_Soldier_TL",
	"RUS_Soldier3",
	"RUS_Soldier2",
	"RUS_Soldier1",
	"RUS_Commander",
	"RUS_Soldier_Marksman",
	"RUS_Soldier_GL",
	"RUS_Soldier_Sab",
	"MVD_Soldier_TL",
	"MVD_Soldier_Sniper",
	"MVD_Soldier_AT",
	"MVD_Soldier_GL",
	"MVD_Soldier",
	"MVD_Soldier_Marksman",
	"MVD_Soldier_MG",
	"RU_Soldier_TL",
	"RU_Soldier_SL",
	"RU_Soldier_Spotter",
	"RU_Soldier_SniperH",
	"RU_Soldier_Sniper",
	"RU_Soldier2",
	"RU_Soldier_AT",
	"RU_Soldier_LAT",
	"RU_Soldier",
	"RU_Soldier_Pilot",
	"RU_Soldier_Officer",
	"RU_Soldier_Medic",
	"RU_Soldier_Marksman",
	"RU_Soldier_MG",
	"RU_Soldier_GL",
	"RU_Commander",
	"RU_Soldier_Crew",
	"RU_Soldier_AR",
	"RU_Soldier_HAT",
	"RU_Soldier_AA",
	"CDF_Soldier_TL",
	"CDF_Soldier_Spotter",
	"CDF_Soldier_Light",
	"CDF_Soldier_Sniper",
	"CDF_Soldier",
	"CDF_Soldier_Pilot",
	"CDF_Soldier_Officer",
	"CDF_Soldier_Militia",
	"CDF_Soldier_Medic",
	"CDF_Soldier_Marksman",
	"CDF_Soldier_MG",
	"CDF_Soldier_GL",
	"CDF_Commander",
	"CDF_Soldier_Engineer",
	"CDF_Soldier_Crew",
	"CDF_Soldier_AR",
	"CDF_Soldier_RPG",
	"CDF_Soldier_Strela",
	"FR_TL",
	"FR_Sykes",
	"FR_R",
	"FR_Rodriguez",
	"FR_OHara",
	"FR_Miles",
	"FR_Marksman",
	"FR_AR",
	"FR_GL",
	"FR_AC",
	"FR_Sapper",
	"FR_Corpsman",
	"FR_Cooper",
	"FR_Commander",
	"FR_Assault_R",
	"FR_Assault_GL",
	"USMC_Soldier_SL",
	"USMC_SoldierS_Spotter",
	"USMC_SoldierS_SniperH",
	"USMC_SoldierS_Sniper",
	"USMC_SoldierS",
	"USMC_Soldier_LAT",
	"USMC_Soldier2",
	"USMC_Soldier",
	"USMC_Soldier_Pilot",
	"USMC_Soldier_Officer",
	"USMC_Soldier_MG",
	"USMC_Soldier_GL",
	"USMC_Soldier_TL",
	"USMC_SoldierS_Engineer",
	"USMC_SoldierM_Marksman",
	"USMC_Soldier_Crew",
	"USMC_Soldier_Medic",
	"USMC_Soldier_AR",
	"USMC_Soldier_AT",
	"USMC_Soldier_HAT",
	"USMC_Soldier_AA"
	];
	
RHQ_Art_A2 = 
	[
	"2b14_82mm_GUE",
	"2b14_82mm_INS",
	"GRAD_INS",
	"2b14_82mm",
	"D30_RU",
	"GRAD_RU",
	"2b14_82mm_CDF",
	"D30_CDF",
	"GRAD_CDF",
	"MLRS",
	"M252",
	"M119"
	];
	
RHQ_HArmor_A2 = 
	[
	"T72_Gue",
	"T34",
	"T72_INS",
	"T90",
	"T72_RU",
	"T72_CDF",
	"M1A1",
	"M1A2_TUSK_MG"
	];
	
RHQ_MArmor_A2 =
	[
	"T34",
	"BMP2_Gue",
	"BMP2_INS",
	"BMP2_CDF",
	"BMP3"
	];
	
RHQ_LArmor_A2 = 
	[
	"BRDM2_HQ_Gue",
	"BRDM2_Gue",
	"BMP2_Gue",
	"ZSU_INS",
	"BRDM2_ATGM_INS",
	"BRDM2_INS",
	"BMP2_HQ_INS",
	"BMP2_INS",
	"GAZ_Vodnik_HMG",
	"GAZ_Vodnik",
	"BTR90_HQ",
	"BTR90",
	"BMP3",
	"2S6M_Tunguska",
	"ZSU_CDF",
	"BRDM2_ATGM_CDF",
	"BRDM2_CDF",
	"BMP2_HQ_CDF",
	"BMP2_CDF",
	"LAV25_HQ",
	"LAV25",
	"AAV"
	];
	
RHQ_LArmorAT_A2 = 
	[
	"BMP2_Gue",
	"BRDM2_ATGM_INS",
	"BMP2_INS",
	"BTR90",
	"BMP3",
	"BRDM2_ATGM_CDF",
	"BMP2_CDF"
	];
	
RHQ_Cars_A2 = 
	[
	"Ural_ZU23_Gue",
	"V3S_Gue",
	"Pickup_PK_GUE",
	"Offroad_SPG9_Gue",
	"Offroad_DSHKM_Gue",
	"TT650_Gue",
	"UralRepair_INS",
	"UralRefuel_INS",
	"UralReammo_INS",
	"BMP2_Ambul_INS",
	"Ural_ZU23_INS",
	"UralOpen_INS",
	"Ural_INS",
	"UAZ_SPG9_INS",
	"UAZ_MG_INS",
	"UAZ_AGS30_INS",
	"UAZ_INS",
	"Pickup_PK_INS",
	"Offroad_DSHKM_INS",
	"TT650_Ins",
	"GRAD_INS",
	"GAZ_Vodnik_MedEvac",
	"KamazRepair",
	"KamazRefuel",
	"KamazReammo",
	"KamazOpen",
	"Kamaz",
	"UAZ_AGS30_RU",
	"UAZ_RU",
	"GRAD_RU",
	"UralRepair_CDF",
	"UralRefuel_CDF",
	"UralReammo_CDF",
	"BMP2_Ambul_CDF",
	"Ural_ZU23_CDF",
	"UralOpen_CDF",
	"Ural_CDF",
	"UAZ_MG_CDF",
	"UAZ_AGS30_CDF",
	"UAZ_CDF",
	"GRAD_CDF",
	"MtvrRepair",
	"MtvrRefuel",
	"MtvrReammo",
	"HMMWV_Ambulance",
	"TowingTractor",
	"MTVR",
	"MMT_USMC",
	"M1030",
	"HMMWV_Avenger",
	"HMMWV_TOW",
	"HMMWV_MK19",
	"HMMWV_Armored",
	"HMMWV_M2",
	"HMMWV"
	];
	
RHQ_Air_A2 = 
	[
	"Mi17_medevac_Ins",
	"Su25_Ins",
	"Mi17_Ins",
	"Mi17_medevac_RU",
	"Su34",
	"Su39",
	"Pchela1T",
	"Mi17_rockets_RU",
	"Mi24_V",
	"Mi24_P",
	"Ka52Black",
	"Ka52",
	"Mi17_medevac_CDF",
	"Su25_CDF",
	"Mi24_D",
	"Mi17_CDF",
	"MV22",
	"C130J",
	"MQ9PredatorB",
	"AH64D",
	"UH1Y",
	"MH60S",
	"F35B",
	"AV8B",
	"AV8B2",
	"AH1Z",
	"A10"
	];
	
RHQ_BAir_A2 = 
	[
	];
	
RHQ_RAir_A2 =
	[
	"Pchela1T",
	"MQ9PredatorB"
	];
	
RHQ_NCAir_A2 = 
	[
	"Mi17_medevac_Ins",
	"Mi17_medevac_RU",
	"Pchela1T",
	"Mi17_medevac_CDF",
	"MV22",
	"C130J",
	"MQ9PredatorB"
	];
	
RHQ_Naval_A2 = 
	[
	"PBX",
	"RHIB2Turret",
	"RHIB",
	"Zodiac"
	];
	
RHQ_Static_A2 = 
	[
	"GUE_WarfareBMGNest_PK",
	"ZU23_Gue",
	"SPG9_Gue",
	"2b14_82mm_GUE",
	"DSHKM_Gue",
	"Ins_WarfareBMGNest_PK",
	"ZU23_Ins",
	"SPG9_Ins",
	"2b14_82mm_INS",
	"DSHkM_Mini_TriPod",
	"DSHKM_Ins",
	"D30_Ins",
	"AGS_Ins",
	"RU_WarfareBMGNest_PK",
	"CDF_WarfareBMGNest_PK",
	"USMC_WarfareBMGNest_M240",
	"2b14_82mm",
	"Metis",
	"KORD",
	"KORD_high",
	"D30_RU",
	"AGS_RU",
	"Igla_AA_pod_East",
	"ZU23_CDF",
	"SPG9_CDF",
	"2b14_82mm_CDF",
	"DSHkM_Mini_TriPod_CDF",
	"DSHKM_CDF",
	"D30_CDF",
	"AGS_CDF",
	"TOW_TriPod",
	"MK19_TriPod",
	"M2HD_mini_TriPod",
	"M252",
	"M2StaticMG",
	"M119",
	"Stinger_Pod",
	"Fort_Nest_M240"
	];
	
RHQ_StaticAA_A2 = 
	[
	"ZU23_Gue",
	"ZU23_Ins",
	"Igla_AA_pod_East",
	"ZU23_CDF",
	"Stinger_Pod"
	];
	
RHQ_StaticAT_A2 = 
	[
	"SPG9_Gue",
	"SPG9_Ins",
	"Metis",
	"SPG9_CDF",
	"TOW_TriPod"
	];
	
RHQ_Support_A2 =
	[
	"UralRepair_INS",
	"UralRefuel_INS",
	"UralReammo_INS",
	"Mi17_medevac_Ins",
	"BMP2_Ambul_INS",
	"GAZ_Vodnik_MedEvac",
	"KamazRepair",
	"KamazRefuel",
	"Mi17_medevac_RU",
	"KamazReammo",
	"UralRepair_CDF",
	"UralRefuel_CDF",
	"UralReammo_CDF",
	"Mi17_medevac_CDF",
	"BMP2_Ambul_CDF",
	"MtvrRepair",
	"MtvrRefuel",
	"MtvrReammo",
	"HMMWV_Ambulance",
	"MH60S"
	];
	
RHQ_Cargo_A2 = 
	[
	"V3S_Gue",
	"Pickup_PK_GUE",
	"Offroad_SPG9_Gue",
	"Offroad_DSHKM_Gue",
	"BRDM2_HQ_Gue",
	"BRDM2_Gue",
	"BMP2_Gue",
	"Mi17_medevac_Ins",
	"BMP2_Ambul_INS",
	"UralOpen_INS",
	"Ural_INS",
	"UAZ_SPG9_INS",
	"UAZ_MG_INS",
	"UAZ_AGS30_INS",
	"UAZ_INS",
	"Pickup_PK_INS",
	"Offroad_DSHKM_INS",
	"BRDM2_ATGM_INS",
	"BRDM2_INS",
	"BMP2_HQ_INS",
	"BMP2_INS",
	"Mi17_Ins",
	"GAZ_Vodnik_MedEvac",
	"Mi17_medevac_RU",
	"PBX",
	"KamazOpen",
	"Kamaz",
	"UAZ_AGS30_RU",
	"UAZ_RU",
	"GAZ_Vodnik_HMG",
	"GAZ_Vodnik",
	"BTR90_HQ",
	"BTR90",
	"BMP3",
	"Mi17_rockets_RU",
	"Mi17_medevac_CDF",
	"BMP2_Ambul_CDF",
	"UralOpen_CDF",
	"Ural_CDF",
	"UAZ_MG_CDF",
	"UAZ_AGS30_CDF",
	"UAZ_CDF",
	"BRDM2_ATGM_CDF",
	"BRDM2_CDF",
	"BMP2_HQ_CDF",
	"BMP2_CDF",
	"Mi17_CDF",
	"HMMWV_Ambulance",
	"RHIB2Turret",
	"RHIB",
	"Zodiac",
	"MTVR",
	"HMMWV_TOW",
	"HMMWV_MK19",
	"HMMWV_Armored",
	"HMMWV_M2",
	"HMMWV",
	"LAV25_HQ",
	"LAV25",
	"AAV",
	"UH1Y",
	"MH60S",
	"MV22",
	"C130J"
	];
	
RHQ_NCCargo_A2 = 
	[
	"V3S_Gue",
	"Mi17_medevac_Ins",
	"BMP2_Ambul_INS",
	"UralOpen_INS",
	"Ural_INS",
	"UAZ_INS",
	"GAZ_Vodnik_MedEvac",
	"Mi17_medevac_RU",
	"PBX",
	"KamazOpen",
	"Kamaz",
	"UAZ_RU",
	"Mi17_medevac_CDF",
	"BMP2_Ambul_CDF",
	"UralOpen_CDF",
	"Ural_CDF",
	"UAZ_CDF",
	"HMMWV_Ambulance",
	"Zodiac",
	"MTVR",
	"HMMWV",
	"MV22",
	"C130J"
	];
	
RHQ_Crew_A2 = 
	[
	"GUE_Soldier_Pilot",
	"INS_Soldier_Pilot",
	"RU_Soldier_Pilot",
	"CDF_Soldier_Pilot",
	"USMC_Soldier_Pilot",
	"GUE_Soldier_Crew",
	"INS_Soldier_Crew",
	"RU_Soldier_Crew",
	"CDF_Soldier_Crew",
	"USMC_Soldier_Crew"
	];

RHQ_SpecFor_OA = 
	[
	"US_Delta_Force_AR_EP1",
	"US_Delta_Force_MG_EP1",
	"US_Delta_Force_Marksman_EP1",
	"US_Delta_Force_Medic_EP1",
	"US_Delta_Force_EP1",
	"US_Delta_Force_Assault_EP1",
	"US_Delta_Force_M14_EP1",
	"US_Delta_Force_Night_EP1",
	"US_Delta_Force_SD_EP1",
	"US_Delta_Force_TL_EP1",
	"US_Delta_Force_Air_Controller_EP1",
	"TK_Special_Forces_MG_EP1",
	"TK_Special_Forces_EP1",
	"TK_Special_Forces_TL_EP1",
	"CZ_Special_Forces_DES_EP1",
	"CZ_Special_Forces_GL_DES_EP1",
	"CZ_Special_Forces_MG_DES_EP1",
	"CZ_Special_Forces_Scout_DES_EP1",
	"CZ_Special_Forces_TL_DES_EP1"
	];

RHQ_SpecFor_ACR = 
	[
	"CZ_Soldier_Spec_Demo_Wdl_ACR",
	"CZ_Soldier_Spec3_Wdl_ACR",
	"CZ_Soldier_Spec2_Wdl_ACR",
	"CZ_Soldier_Spec1_Wdl_ACR"
	];

RHQ_SpecFor_BAF = 
	[
	];

RHQ_SpecFor_PMC = 
	[
	];

RHQ_Recon_OA = 
	[
	"GER_Soldier_Scout_EP1",
	"US_Soldier_Spotter_EP1",
	"TK_Soldier_Spotter_EP1"
	];

RHQ_Recon_ACR = 
	[
	"CZ_Soldier_Spotter_ACR",
	"CZ_Soldier_Recon_Wdl_ACR"
	];

RHQ_Recon_BAF = 
	[
	"BAF_Soldier_spotter_W",
	"BAF_Soldier_spotterN_W"
	];

RHQ_Recon_PMC = 
	[
	];

RHQ_FO_OA = 
	[
	"GER_Soldier_Scout_EP1",
	"US_Soldier_Spotter_EP1",
	"TK_Soldier_Spotter_EP1"
	];

RHQ_FO_ACR = 
	[
	"CZ_Soldier_Spotter_ACR"
	];

RHQ_FO_BAF = 
	[
	"BAF_Soldier_spotter_W",
	"BAF_Soldier_spotterN_W"
	];

RHQ_FO_PMC = 
	[
	];

RHQ_Snipers_OA = 
	[
	"CZ_Soldier_Sniper_EP1",
	"US_Soldier_Marksman_EP1",
	"TK_Soldier_Sniper_EP1",
	"TK_Soldier_SniperH_EP1",
	"TK_Soldier_Sniper_Night_EP1",
	"TK_INS_Soldier_Sniper_EP1",
	"TK_GUE_Soldier_Sniper_EP1",
	"US_Soldier_Sniper_EP1",
	"US_Soldier_SniperH_EP1",
	"US_Soldier_Sniper_NV_EP1"
	];

RHQ_Snipers_ACR = 
	[
	"CZ_Soldier_Sniper_ACR",
	"CZ_Sharpshooter_Wdl_ACR",
	"CZ_Sharpshooter_DES_ACR"
	];

RHQ_Snipers_BAF = 
	[
	"BAF_Soldier_SniperN_MTP",
	"BAF_Soldier_SniperH_MTP",
	"BAF_Soldier_Sniper_MTP",
	"BAF_Soldier_SniperN_W",
	"BAF_Soldier_SniperH_W",
	"BAF_Soldier_Sniper_W",
	"BAF_Soldier_Marksman_DDPM"
	];

RHQ_Snipers_PMC = 
	[
	"Soldier_Sniper_PMC",
	"Soldier_Sniper_KSVK_PMC"
	];

RHQ_ATInf_OA = 
	[
	"TK_GUE_Soldier_HAT_EP1",
	"TK_GUE_Soldier_AT_EP1",
	"US_Soldier_AT_EP1",
	"US_Soldier_HAT_EP1",
	"US_Soldier_LAT_EP1",
	"TK_Soldier_HAT_EP1",
	"TK_Soldier_LAT_EP1",
	"TK_Soldier_AT_EP1",
	"TK_INS_Soldier_AT_EP1",
	"UN_CDF_Soldier_AT_EP1"
	];

RHQ_ATInf_ACR = 
	[
	"CZ_Soldier_RPG_Wdl_ACR",
	"CZ_Soldier_AT_Wdl_ACR"
	];

RHQ_ATInf_BAF = 
	[
	"BAF_Soldier_AT_MTP",
	"BAF_Soldier_HAT_MTP",
	"BAF_Soldier_AT_DDPM",
	"BAF_Soldier_HAT_DDPM",
	"BAF_Soldier_AT_W",
	"BAF_Soldier_HAT_W"
	];

RHQ_ATInf_PMC = 
	[
	"Soldier_AT_PMC"
	];

RHQ_AAInf_OA = 
	[
	"M6_EP1",
	"ZSU_TK_EP1",
	"HMMWV_Avenger_DES_EP1",
	"Ural_ZU23_TK_GUE_EP1",
	"Ural_ZU23_TK_EP1",
	"TK_GUE_Soldier_AA_EP1",
	"US_Soldier_AA_EP1",
	"TK_Soldier_AA_EP1",
	"TK_INS_Soldier_AA_EP1"
	];

RHQ_AAInf_ACR = 
	[
	];

RHQ_AAInf_BAF = 
	[
	"BAF_Soldier_AT_MTP",
	"BAF_Soldier_HAT_MTP",
	"BAF_Soldier_AT_DDPM",
	"BAF_Soldier_HAT_DDPM",
	"BAF_Soldier_AT_W",
	"BAF_Soldier_HAT_W"
	];

RHQ_AAInf_PMC = 
	[
	"Soldier_AA_PMC"
	];

RHQ_Inf_OA = 
	[
	"TK_GUE_Soldier_TL_EP1",
	"TK_GUE_Soldier_MG_EP1",
	"TK_GUE_Soldier_4_EP1",
	"TK_GUE_Soldier_Sniper_EP1",
	"TK_GUE_Soldier_5_EP1",
	"TK_GUE_Soldier_AT_EP1",
	"TK_GUE_Soldier_2_EP1",
	"TK_GUE_Soldier_HAT_EP1",
	"TK_GUE_Soldier_AT_EP1",
	"TK_GUE_Soldier_AA_EP1",
	"TK_GUE_Soldier_EP1",
	"TK_GUE_Soldier_AR_EP1",
	"TK_GUE_Soldier_3_EP1",
	"TK_GUE_Bonesetter_EP1",
	"TK_GUE_Warlord_EP1",
	"TK_CIV_Takistani03_EP1",
	"US_Soldier_AA_EP1",
	"US_Soldier_AT_EP1",
	"US_Soldier_AAT_EP1",
	"US_Soldier_HAT_EP1",
	"US_Soldier_AHAT_EP1",
	"US_Soldier_AR_EP1",
	"US_Soldier_AAR_EP1",
	"US_Soldier_Crew_EP1",
	"US_Soldier_Engineer_EP1",
	"US_Soldier_GL_EP1",
	"US_Soldier_MG_EP1",
	"US_Soldier_AMG_EP1",
	"US_Soldier_Marksman_EP1",
	"US_Soldier_Medic_EP1",
	"US_Soldier_Officer_EP1",
	"US_Soldier_Pilot_EP1",
	"US_Pilot_Light_EP1",
	"US_Soldier_EP1",
	"US_Soldier_LAT_EP1",
	"US_Soldier_B_EP1",
	"US_Soldier_Sniper_EP1",
	"US_Soldier_SniperH_EP1",
	"US_Soldier_Sniper_NV_EP1",
	"US_Soldier_Light_EP1",
	"US_Soldier_Spotter_EP1",
	"US_Soldier_SL_EP1",
	"US_Soldier_TL_EP1",
	"TK_Soldier_AA_EP1",
	"TK_Soldier_AAT_EP1",
	"TK_Soldier_AMG_EP1",
	"TK_Soldier_HAT_EP1",
	"TK_Soldier_AR_EP1",
	"TK_Aziz_EP1",
	"TK_Commander_EP1",
	"TK_Soldier_Crew_EP1",
	"TK_Soldier_Engineer_EP1",
	"TK_Soldier_GL_EP1",
	"TK_Soldier_MG_EP1",
	"TK_Soldier_Medic_EP1",
	"TK_Soldier_Officer_EP1",
	"TK_Soldier_Pilot_EP1",
	"TK_Soldier_EP1",
	"TK_Soldier_B_EP1",
	"TK_Soldier_LAT_EP1",
	"TK_Soldier_AT_EP1",
	"TK_Soldier_Sniper_EP1",
	"TK_Soldier_SniperH_EP1",
	"TK_Soldier_Sniper_Night_EP1",
	"TK_Soldier_Night_1_EP1",
	"TK_Soldier_Night_2_EP1",
	"TK_Soldier_TWS_EP1",
	"TK_Soldier_Spotter_EP1",
	"TK_Soldier_SL_EP1",
	"TK_Special_Forces_MG_EP1",
	"TK_Special_Forces_EP1",
	"TK_Special_Forces_TL_EP1",
	"TK_INS_Soldier_AA_EP1",
	"TK_INS_Soldier_AR_EP1",
	"TK_INS_Bonesetter_EP1",
	"TK_INS_Soldier_MG_EP1",
	"TK_INS_Soldier_2_EP1",
	"TK_INS_Soldier_EP1",
	"TK_INS_Soldier_4_EP1",
	"TK_INS_Soldier_3_EP1",
	"TK_INS_Soldier_AAT_EP1",
	"TK_INS_Soldier_Sniper_EP1",
	"TK_INS_Soldier_TL_EP1",
	"TK_INS_Warlord_EP1",
	"TK_INS_Soldier_AT_EP1",
	"TK_GUE_Soldier_AAT_EP1",
	"GER_Soldier_MG_EP1",
	"GER_Soldier_Medic_EP1",
	"GER_Soldier_EP1",
	"GER_Soldier_Scout_EP1",
	"GER_Soldier_TL_EP1",
	"US_Delta_Force_AR_EP1",
	"US_Delta_Force_M14_EP1",
	"US_Delta_Force_MG_EP1",
	"US_Delta_Force_EP1",
	"US_Delta_Force_Assault_EP1",
	"US_Delta_Force_Marksman_EP1",
	"US_Delta_Force_Air_Controller_EP1",
	"US_Delta_Force_Medic_EP1",
	"US_Delta_Force_Night_EP1",
	"CZ_Soldier_AMG_DES_EP1",
	"CZ_Soldier_AT_DES_EP1",
	"CZ_Soldier_B_DES_EP1",
	"CZ_Soldier_DES_EP1",
	"CZ_Soldier_Light_DES_EP1",
	"CZ_Soldier_MG_DES_EP1",
	"CZ_Soldier_Office_DES_EP1",
	"CZ_Soldier_Pilot_EP1",
	"CZ_Soldier_SL_DES_EP1",
	"CZ_Soldier_Sniper_EP1",
	"CZ_Special_Forces_DES_EP1",
	"CZ_Special_Forces_GL_DES_EP1",
	"CZ_Special_Forces_MG_DES_EP1",
	"CZ_Special_Forces_Scout_DES_EP1",
	"CZ_Special_Forces_TL_DES_EP1",
	"Drake",
	"Drake_Light",
	"Graves",
	"Graves_Light",
	"Herrera",
	"Herrera_Light",
	"Pierce",
	"Pierce_Light",
	"UN_CDF_Soldier_AAT_EP1",
	"UN_CDF_Soldier_AMG_EP1",
	"UN_CDF_Soldier_Crew_EP1",
	"UN_CDF_Soldier_B_EP1",
	"UN_CDF_Soldier_AT_EP1",
	"UN_CDF_Soldier_Light_EP1",
	"UN_CDF_Soldier_SL_EP1",
	"UN_CDF_Soldier_Guard_EP1",
	"UN_CDF_Soldier_MG_EP1",
	"UN_CDF_Soldier_Officer_EP1",
	"UN_CDF_Soldier_Pilot_EP1",
	"UN_CDF_Soldier_EP1"
	];

RHQ_Inf_ACR = 
	[
	"CZ_Soldier_RPG_Ass_Wdl_ACR",
	"CZ_Soldier_MG2_Wdl_ACR",
	"CZ_Soldier_Crew_Wdl_ACR",
	"CZ_Soldier_Engineer_Wdl_ACR",
	"CZ_Soldier_805g_Wdl_ACR",
	"CZ_Soldier_MG_Wdl_ACR",
	"CZ_Sharpshooter_Wdl_ACR",
	"CZ_Soldier_Medic_Wdl_ACR",
	"CZ_Soldier_Officer_Wdl_ACR",
	"CZ_Soldier_Pilot_Wdl_ACR",
	"CZ_Soldier_Wdl_ACR",
	"CZ_Soldier_AT_Wdl_ACR",
	"CZ_Soldier_805_Wdl_ACR",
	"CZ_Soldier_RPG_Wdl_ACR",
	"CZ_Soldier_Sniper_ACR",
	"CZ_Soldier_Light_Wdl_ACR",
	"CZ_Soldier_Spotter_ACR",
	"CZ_Soldier_Leader_Wdl_ACR",
	"CZ_Soldier_Spec3_Wdl_ACR",
	"CZ_Soldier_Spec2_Wdl_ACR",
	"CZ_Soldier_Recon_Wdl_ACR",
	"CZ_Soldier_Spec_Demo_Wdl_ACR",
	"CZ_Soldier_Spec1_Wdl_ACR",
	"CZ_Soldier_Crew_Dst_ACR",
	"CZ_Soldier_RPG_Dst_ACR",
	"CZ_Soldier_medik_DES_EP1",
	"CZ_Sharpshooter_DES_ACR",
	"CZ_Soldier_MG2_Dst_ACR",
	"CZ_Soldier_RPG_Ass_Dst_ACR",
	"CZ_Soldier_805g_Dst_ACR",
	"CZ_Soldier_Spec_Demo_Dst_ACR",
	"CZ_Soldier_Engineer_Dst_ACR",
	"CZ_Soldier805_DES_ACR"
	];

RHQ_Inf_BAF = 
	[
	"BAF_Soldier_AA_MTP",
	"BAF_Soldier_AAA_MTP",
	"BAF_Soldier_AAT_MTP",
	"BAF_Soldier_AHAT_MTP",
	"BAF_Soldier_AAR_MTP",
	"BAF_Soldier_AMG_MTP",
	"BAF_Soldier_AT_MTP",
	"BAF_Soldier_HAT_MTP",
	"BAF_Soldier_AR_MTP",
	"BAF_crewman_MTP",
	"BAF_Soldier_EN_MTP",
	"BAF_Soldier_GL_MTP",
	"BAF_Soldier_FAC_MTP",
	"BAF_Soldier_MG_MTP",
	"BAF_Soldier_scout_MTP",
	"BAF_Soldier_Marksman_MTP",
	"BAF_Soldier_Medic_MTP",
	"BAF_Soldier_Officer_MTP",
	"BAF_Pilot_MTP",
	"BAF_Soldier_MTP",
	"BAF_ASoldier_MTP",
	"BAF_Soldier_L_MTP",
	"BAF_Soldier_N_MTP",
	"BAF_Soldier_SL_MTP",
	"BAF_Soldier_SniperN_MTP",
	"BAF_Soldier_SniperH_MTP",
	"BAF_Soldier_Sniper_MTP",
	"BAF_Soldier_spotter_MTP",
	"BAF_Soldier_spotterN_MTP",
	"BAF_Soldier_TL_MTP",
	"BAF_Soldier_AA_DDPM",
	"BAF_Soldier_AAA_DDPM",
	"BAF_Soldier_AAT_DDPM",
	"BAF_Soldier_AHAT_DDPM",
	"BAF_Soldier_AAR_DDPM",
	"BAF_Soldier_AMG_DDPM",
	"BAF_Soldier_AT_DDPM",
	"BAF_Soldier_HAT_DDPM",
	"BAF_Soldier_AR_DDPM",
	"BAF_crewman_DDPM",
	"BAF_Soldier_EN_DDPM",
	"BAF_Soldier_GL_DDPM",
	"BAF_Soldier_FAC_DDPM",
	"BAF_Soldier_MG_DDPM",
	"BAF_Soldier_scout_DDPM",
	"BAF_Soldier_Marksman_DDPM",
	"BAF_Soldier_Medic_DDPM",
	"BAF_Soldier_Officer_DDPM",
	"BAF_Pilot_DDPM",
	"BAF_Soldier_DDPM",
	"BAF_ASoldier_DDPM",
	"BAF_Soldier_L_DDPM",
	"BAF_Soldier_N_DDPM",
	"BAF_Soldier_SL_DDPM",
	"BAF_Soldier_TL_DDPM",
	"BAF_Soldier_AA_W",
	"BAF_Soldier_AAA_W",
	"BAF_Soldier_AAT_W",
	"BAF_Soldier_AHAT_W",
	"BAF_Soldier_AAR_W",
	"BAF_Soldier_AMG_W",
	"BAF_Soldier_AT_W",
	"BAF_Soldier_HAT_W",
	"BAF_Soldier_AR_W",
	"BAF_crewman_W",
	"BAF_Soldier_EN_W",
	"BAF_Soldier_GL_W",
	"BAF_Soldier_FAC_W",
	"BAF_Soldier_MG_W",
	"BAF_Soldier_scout_W",
	"BAF_Soldier_Marksman_W",
	"BAF_Soldier_Medic_W",
	"BAF_Soldier_Officer_W",
	"BAF_Pilot_W",
	"BAF_Soldier_W",
	"BAF_ASoldier_W",
	"BAF_Soldier_L_W",
	"BAF_Soldier_N_W",
	"BAF_Soldier_SL_W",
	"BAF_Soldier_SniperN_W",
	"BAF_Soldier_SniperH_W",
	"BAF_Soldier_Sniper_W",
	"BAF_Soldier_spotter_W",
	"BAF_Soldier_spotterN_W",
	"BAF_Soldier_TL_W"
	];

RHQ_Inf_PMC = 
	[
	"CIV_Contractor1_BAF",
	"CIV_Contractor2_BAF",
	"Soldier_Bodyguard_M4_PMC",
	"Soldier_Bodyguard_AA12_PMC",
	"Soldier_Bodyguard_M4_PMC",
	"Soldier_Sniper_PMC",
	"Soldier_Medic_PMC",
	"Soldier_MG_PMC",
	"Soldier_MG_PKM_PMC",
	"Soldier_AT_PMC",
	"Soldier_Engineer_PMC",
	"Soldier_GL_M16A2_PMC",
	"Soldier_M4A3_PMC",
	"Soldier_PMC",
	"Soldier_GL_PMC",
	"Soldier_Crew_PMC",
	"Soldier_Pilot_PMC",
	"Soldier_Sniper_KSVK_PMC",
	"Soldier_AA_PMC",
	"Soldier_TL_PMC",
	"Soldier_Sniper_PMC"
	];

RHQ_Art_OA = 
	[
	"D30_TK_GUE_EP1",
	"2b14_82mm_TK_GUE_EP1",
	"M1129_MC_EP1",
	"MLRS_DES_EP1",
	"M252_US_EP1",
	"M119_US_EP1",
	"GRAD_TK_EP1",
	"MAZ_543_SCUD_TK_EP1",
	"2b14_82mm_TK_EP1",
	"D30_TK_EP1",
	"2b14_82mm_TK_INS_EP1",
	"2b14_82mm_CZ_EP1"
	];

RHQ_Art_ACR = 
	[
	"RM70_ACR"
	];

RHQ_Art_BAF = 
	[
	];

RHQ_Art_PMC = 
	[
	];

RHQ_HArmor_OA = 
	[
	"T55_TK_GUE_EP1",
	"T34_TK_GUE_EP1",
	"M1A1_US_DES_EP1",
	"M1A2_US_TUSK_MG_EP1",
	"T34_TK_EP1",
	"T72_TK_EP1",
	"T55_TK_EP1"
	];

RHQ_HArmor_ACR = 
	[
	"T72_ACR"
	];

RHQ_HArmor_BAF = 
	[
	];

RHQ_HArmor_PMC = 
	[
	];

RHQ_MArmor_OA = 
	[
	"M2A2_EP1",
	"M2A3_EP1",
	"T34_TK_EP1",
	"T34_TK_GUE_EP1"
	];

RHQ_MArmor_ACR = 
	[
	"Pandur2_ACR",
	"BMP2_Des_ACR",
	"BMP2_ACR",
	"BVP1_TK_ACR"
	];

RHQ_MArmor_BAF = 
	[
	"BAF_FV510_D",
	"BAF_FV510_W"
	];

RHQ_MArmor_PMC = 
	[
	];

RHQ_LArmor_OA = 
	[
	"BRDM2_TK_GUE_EP1",
	"BRDM2_HQ_TK_GUE_EP1",
	"BRDM2_ATGM_TK_EP1",
	"M1130_CV_EP1",
	"M6_EP1",
	"M2A2_EP1",
	"M2A3_EP1",
	"M1126_ICV_M2_EP1",
	"M1126_ICV_mk19_EP1",
	"M1135_ATGMV_EP1",
	"M1128_MGS_EP1",
	"BMP2_TK_EP1",
	"BMP2_HQ_TK_EP1",
	"BMP2_UN_EP1",
	"BRDM2_TK_EP1",
	"BTR60_TK_EP1",
	"M113_TK_EP1",
	"ZSU_TK_EP1",
	"M113_UN_EP1"
	];

RHQ_LArmor_ACR = 
	[
	"BRDM2_Desert_ACR",
	"BRDM2_ACR",
	"BMP2_Des_ACR",
	"BMP2_ACR",
	"BVP1_TK_ACR",
	"Dingo_GL_Wdl_ACR",
	"Dingo_WDL_ACR",
	"Dingo_DST_ACR",
	"Dingo_GL_DST_ACR",
	"Pandur2_ACR"
	];

RHQ_LArmor_BAF = 
	[
	"BAF_FV510_D",
	"BAF_FV510_W"
	];

RHQ_LArmor_PMC = 
	[
	];

RHQ_LArmorAT_OA = 
	[
	"M2A2_EP1",
	"M2A3_EP1",
	"M1135_ATGMV_EP1",
	"M1128_MGS_EP1",
	"BMP2_TK_EP1",
	"BMP2_UN_EP1"
	];

RHQ_LArmorAT_ACR = 
	[
	"BMP2_Des_ACR",
	"BVP1_TK_ACR",
	"Pandur2_ACR",
	"BVP1_TK_GUE_ACR"
	];

RHQ_LArmorAT_BAF = 
	[
	];

RHQ_LArmorAT_PMC = 
	[
	];

RHQ_Cars_OA = 
	[
	"UralRefuel_TK_EP1",
	"UralReammo_TK_EP1",
	"UralRepair_TK_EP1",
	"BTR40_TK_GUE_EP1",
	"BTR40_MG_TK_GUE_EP1",
	"Pickup_PK_TK_GUE_EP1",
	"Offroad_SPG9_TK_GUE_EP1",
	"Offroad_DSHKM_TK_GUE_EP1",
	"V3S_TK_GUE_EP1",
	"V3S_Reammo_TK_GUE_EP1",
	"ATV_US_EP1",
	"ATV_CZ_EP1",
	"HMMWV_DES_EP1",
	"HMMWV_MK19_DES_EP1",
	"HMMWV_TOW_DES_EP1",
	"HMMWV_M998_crows_M2_DES_EP1",
	"HMMWV_M998_crows_MK19_DES_EP1",
	"HMMWV_M1151_M2_DES_EP1",
	"HMMWV_M998A2_SOV_DES_EP1",
	"HMMWV_Terminal_EP1",
	"HMMWV_M1035_DES_EP1",
	"HMMWV_Avenger_DES_EP1",
	"HMMWV_M1151_M2_CZ_DES_EP1",
	"M1030_US_DES_EP1",
	"MTVR_DES_EP1",
	"LandRover_MG_TK_EP1",
	"LandRover_SPG9_TK_EP1",
	"LandRover_Special_CZ_EP1",
	"TT650_TK_EP1",
	"SUV_TK_EP1",
	"UAZ_Unarmed_TK_EP1",
	"UAZ_AGS30_TK_EP1",
	"UAZ_MG_TK_EP1",
	"Ural_ZU23_TK_EP1",
	"V3S_TK_EP1",
	"V3S_Open_TK_EP1",
	"BTR40_TK_INS_EP1",
	"BTR40_MG_TK_INS_EP1",
	"LandRover_MG_TK_INS_EP1",
	"LandRover_SPG9_TK_INS_EP1",
	"LandRover_CZ_EP1",
	"Old_bike_TK_INS_EP1",
	"Ural_ZU23_TK_GUE_EP1",
	"SUV_UN_EP1",
	"UAZ_Unarmed_UN_EP1",
	"Ural_UN_EP1"
	];

RHQ_Cars_ACR = 
	[
	"M1114_DSK_ACR",
	"LandRover_Ambulance_ACR",
	"LandRover_ACR",
	"RM70_ACR",
	"T810_ACR",
	"T810A_MG_ACR",
	"T810_Open_ACR",
	"T810Reammo_ACR",
	"T810Refuel_ACR",
	"T810Repair_ACR",
	"T810_Des_ACR",
	"UAZ_Unarmed_ACR",
	"M1114_AGS_ACR",
	"T810_Open_Des_ACR",
	"T810Refuel_Des_ACR",
	"LandRover_Ambulance_Des_ACR"
	];

RHQ_Cars_BAF = 
	[
	"BAF_ATV_D",
	"BAF_Jackal2_GMG_D",
	"BAF_Jackal2_L2A1_D",
	"BAF_Offroad_D",
	"BAF_ATV_W",
	"BAF_Jackal2_GMG_W",
	"BAF_Jackal2_L2A1_W",
	"BAF_Offroad_W"
	];

RHQ_Cars_PMC = 
	[
	"SUV_PMC",
	"SUV_PMC_BAF",
	"ArmoredSUV_PMC"
	];

RHQ_Air_OA = 
	[
	"UH1H_TK_GUE_EP1",
	"A10_US_EP1",
	"AH64D_EP1",
	"AH6J_EP1",
	"AH6X_EP1",
	"C130J_US_EP1",
	"CH_47F_EP1",
	"MH6J_EP1",
	"MQ9PredatorB_US_EP1",
	"UH60M_EP1",
	"An2_TK_EP1",
	"L39_TK_EP1",
	"Mi24_D_TK_EP1",
	"Mi17_TK_EP1",
	"Su25_TK_EP1",
	"UH1H_TK_EP1",
	"Mi171Sh_CZ_EP1",
	"Mi171Sh_rockets_CZ_EP1",
	"Mi17_UN_CDF_EP1"
	];

RHQ_Air_ACR = 
	[
	"L159_ACR",
	"L39_ACR",
	"L39_2_ACR",
	"Mi24_D_CZ_ACR"
	];

RHQ_Air_BAF = 
	[
	"BAF_Apache_AH1_D",
	"CH_47F_BAF",
	"BAF_Merlin_HC3_D",
	"AW159_Lynx_BAF"
	];

RHQ_Air_PMC = 
	[
	"Ka137_PMC",
	"Ka137_MG_PMC",
	"Ka60_PMC",
	"Ka60_GL_PMC"
	];

RHQ_BAir_OA = 
	[
	];

RHQ_BAir_ACR = 
	[
	];

RHQ_BAir_BAF = 
	[
	];

RHQ_BAir_PMC = 
	[
	];

RHQ_RAir_OA = 
	[
	"AH6J_EP1",
	"AH6X_EP1",
	"MQ9PredatorB_US_EP1",
	"An2_TK_EP1"
	];

RHQ_RAir_ACR = 
	[
	];

RHQ_RAir_BAF = 
	[
	];

RHQ_RAir_PMC = 
	[
	"Ka137_PMC"
	];

RHQ_NCAir_OA = 
	[
	"C130J_US_EP1",
	"AH6X_EP1",
	"MH6J_EP1",
	"An2_TK_EP1"
	];

RHQ_NCAir_ACR = 
	[
	];

RHQ_NCAir_BAF = 
	[
	];

RHQ_NCAir_PMC = 
	[
	"Ka137_PMC"
	];

RHQ_Naval_OA = 
	[
	"SeaFox_EP1"
	];

RHQ_Naval_ACR = 
	[
	"PBX_ACR"
	];

RHQ_Naval_BAF = 
	[
	];

RHQ_Naval_PMC = 
	[
	];

RHQ_Static_OA = 
	[
	"AGS_TK_GUE_EP1",
	"D30_TK_GUE_EP1",
	"2b14_82mm_TK_GUE_EP1",
	"DSHKM_TK_GUE_EP1",
	"DSHkM_Mini_TriPod_TK_GUE_EP1",
	"SearchLight_TK_EP1",
	"SearchLight_TK_GUE_EP1",
	"SearchLight_US_EP1",
	"SearchLight_TK_INS_EP1",
	"SPG9_TK_GUE_EP1",
	"ZU23_TK_GUE_EP1",
	"M252_US_EP1",
	"M119_US_EP1",
	"M2StaticMG_US_EP1",
	"M2HD_mini_TriPod_US_EP1",
	"MK19_TriPod_US_EP1",
	"TOW_TriPod_US_EP1",
	"Igla_AA_pod_TK_EP1",
	"AGS_TK_EP1",
	"D30_TK_EP1",
	"KORD_high_TK_EP1",
	"KORD_TK_EP1",
	"Metis_TK_EP1",
	"2b14_82mm_TK_EP1",
	"ZU23_TK_EP1",
	"AGS_TK_INS_EP1",
	"D30_TK_INS_EP1",
	"DSHKM_TK_INS_EP1",
	"DSHkM_Mini_TriPod_TK_INS_EP1",
	"2b14_82mm_TK_INS_EP1",
	"SPG9_TK_INS_EP1",
	"ZU23_TK_INS_EP1",
	"WarfareBMGNest_PK_TK_EP1",
	"WarfareBMGNest_PK_TK_GUE_EP1",
	"AGS_UN_EP1",
	"AGS_CZ_EP1",
	"KORD_high_UN_EP1",
	"KORD_UN_EP1",
	"SearchLight_UN_EP1"
	];

RHQ_Static_ACR = 
	[
	"Rbs70_ACR"
	];

RHQ_Static_BAF = 
	[
	"BAF_GMG_Tripod_D",
	"BAF_GPMG_Minitripod_D",
	"BAF_L2A1_Minitripod_D",
	"BAF_L2A1_Tripod_D",
	"BAF_GMG_Tripod_W",
	"BAF_GPMG_Minitripod_W",
	"BAF_L2A1_Minitripod_W",
	"BAF_L2A1_Tripod_W"
	];

RHQ_Static_PMC = 
	[
	];

RHQ_StaticAA_OA = 
	[
	"ZU23_TK_GUE_EP1",
	"Stinger_Pod_US_EP1",
	"Igla_AA_pod_TK_EP1",
	"ZU23_TK_EP1",
	"ZU23_TK_INS_EP1"
	];

RHQ_StaticAA_ACR = 
	[
	"Rbs70_ACR"
	];

RHQ_StaticAA_BAF = 
	[
	];

RHQ_StaticAA_PMC = 
	[
	];

RHQ_StaticAT_OA = 
	[
	"SPG9_TK_GUE_EP1",
	"TOW_TriPod_US_EP1",
	"Metis_TK_EP1",
	"SPG9_TK_INS_EP1"
	];

RHQ_StaticAT_ACR = 
	[
	];

RHQ_StaticAT_BAF = 
	[
	];

RHQ_StaticAT_PMC = 
	[
	];

RHQ_Support_OA = 
	[
	"V3S_Reammo_TK_GUE_EP1",
	"V3S_Refuel_TK_GUE_EP1",
	"V3S_Repair_TK_GUE_EP1",
	"HMMWV_Ambulance_DES_EP1",
	"HMMWV_Ambulance_CZ_DES_EP1",
	"MtvrReammo_DES_EP1",
	"MtvrRefuel_DES_EP1",
	"MtvrRepair_DES_EP1",
	"M1133_MEV_EP1",
	"UH60M_MEV_EP1",
	"M113Ambul_TK_EP1",
	"UralSupply_TK_EP1",
	"UralReammo_TK_EP1",
	"UralRefuel_TK_EP1",
	"UralRepair_TK_EP1",
	"M113Ambul_UN_EP1"
	];

RHQ_Support_ACR = 
	[
	"LandRover_Ambulance_ACR",
	"T810Reammo_ACR",
	"T810Refuel_ACR",
	"T810Repair_ACR",
	"T810Repair_Des_ACR",
	"T810Reammo_Des_ACR",
	"T810Refuel_Des_ACR"
	];

RHQ_Support_BAF = 
	[
	];

RHQ_Support_PMC = 
	[
	];

RHQ_Cargo_OA = 
	[
	"UH1H_TK_GUE_EP1",
	"M1126_ICV_M2_EP1",
	"M1126_ICV_mk19_EP1",
	"M6_EP1","M2A2_EP1",
	"M2A3_EP1",
	"BTR60_TK_EP1",
	"M113_TK_EP1",
	"BMP2_TK_EP1",
	"V3S_TK_EP1",
	"V3S_Open_TK_EP1",
	"SUV_TK_EP1",
	"UAZ_Unarmed_TK_EP1",
	"BTR40_TK_INS_EP1",
	"CH_47F_EP1",
	"UH60M_EP1",
	"BRDM2_ATGM_TK_EP1",
	"LandRover_Special_CZ_EP1",
	"Mi171Sh_CZ_EP1",
	"Mi171Sh_rockets_CZ_EP1"
	];

RHQ_Cargo_ACR = 
	[
	"BMP2_ACR",
	"BRDM2_ACR",
	"BRDM2_Desert_ACR",
	"BRDM2_Desert_ACR",
	"BRDM2_ACR",
	"Dingo_GL_Wdl_ACR",
	"Dingo_WDL_ACR",
	"Dingo_DST_ACR",
	"Dingo_GL_DST_ACR",
	"LandRover_ACR",
	"T810_ACR",
	"T810A_MG_ACR",
	"T810_Open_ACR",
	"T810A_Des_MG_ACR",
	"Mi24_D_CZ_ACR"
	];

RHQ_Cargo_BAF = 
	[
	"BAF_Offroad_D",
	"BAF_Offroad_W",
	"BAF_FV510_D",
	"BAF_FV510_W"
	];

RHQ_Cargo_PMC = 
	[
	"SUV_PMC",
	"SUV_PMC_BAF",
	"ArmoredSUV_PMC",
	"Ka60_PMC",
	"Ka60_GL_PMC"
	];

RHQ_NCCargo_OA = 
	[
	"BTR40_TK_GUE_EP1",
	"V3S_TK_GUE_EP1",
	"MH6J_EP1",
	"HMMWV_DES_EP1",
	"MTVR_DES_EP1",
	"SUV_TK_EP1",
	"UAZ_Unarmed_TK_EP1",
	"V3S_TK_EP1",
	"V3S_Open_TK_EP1",
	"SUV_UN_EP1",
	"UAZ_Unarmed_UN_EP1",
	"Ural_UN_EP1"
	];

RHQ_NCCargo_ACR = 
	[
	"LandRover_ACR",
	"T810_ACR",
	"T810_Open_ACR",
	"T810_Open_Des_ACR",
	"UAZ_Unarmed_ACR"
	];

RHQ_NCCargo_BAF = 
	[
	];

RHQ_NCCargo_PMC = 
	[
	"SUV_PMC",
	"SUV_PMC_BAF",
	"Ka60_PMC"
	];

RHQ_Crew_OA = 
	[
	"US_Soldier_Crew_EP1",
	"US_Soldier_Pilot_EP1",
	"US_Pilot_Light_EP1",
	"TK_Soldier_Crew_EP1",
	"TK_Soldier_Pilot_EP1",
	"CZ_Soldier_Pilot_EP1",
	"UN_CDF_Soldier_Pilot_EP1"
	];

RHQ_Crew_ACR = 
	[
	"CZ_Soldier_Crew_Wdl_ACR",
	"CZ_Soldier_Pilot_Wdl_ACR",
	"CZ_Soldier_Crew_Dst_ACR"
	];

RHQ_Crew_BAF = 
	[
	"BAF_crewman_MTP",
	"BAF_Pilot_MTP",
	"BAF_crewman_DDPM",
	"BAF_Pilot_DDPM",
	"BAF_creWman_W",
	"BAF_Pilot_W"
	];

RHQ_Crew_PMC = 
	[
	"Soldier_Crew_PMC",
	"Soldier_Pilot_PMC"
	];
	
RHQ_specFor_A3 = 
	[

	];
	
RHQ_recon_A3 =
	[
	"O_recon_exp_F",
	"O_recon_F",
	"O_recon_JTAC_F",
	"O_recon_LAT_F",
	"O_recon_M_F",
	"O_recon_medic_F",
	"O_recon_TL_F",
	"B_recon_exp_F",
	"B_recon_F",
	"B_recon_JTAC_F",
	"B_recon_LAT_F",
	"B_recon_M_F",
	"B_recon_medic_F",
	"B_recon_TL_F",
	"I_UAV_AI",
	"O_UAV_AI",
	"B_UAV_AI",
	"I_UAV_01_F",
	"I_UAV_02_CAS_F",
	"I_UAV_02_F",
	"I_UGV_01_F",
	"I_UGV_01_rcws_F",
	"O_UAV_01_F",
	"O_UAV_02_CAS_F",
	"O_UAV_02_F",
	"O_UGV_01_F",
	"O_UGV_01_rcws_F",
	"B_UAV_01_F",
	"B_UAV_02_CAS_F",
	"B_UAV_02_F",
	"B_UGV_01_F",
	"B_UGV_01_rcws_F"
	];
	
RHQ_FO_A3 =
	[
	"I_Spotter_F",
	"O_spotter_F",
	"B_spotter_F",
	"O_recon_JTAC_F",
	"B_recon_JTAC_F"
	];
	
RHQ_snipers_A3 = 
	[
	"I_Sniper_F",
	"O_sniper_F",
	"B_sniper_F",
	"I_Soldier_M_F",
	"O_soldier_M_F",
	"B_G_Soldier_M_F",
	"B_soldier_M_F",
	"O_recon_M_F",
	"B_recon_M_F",
	"O_soldierU_M_F"
	];
	
RHQ_ATinf_A3 = 
	[
	"I_Soldier_AT_F",
	"I_Soldier_LAT_F",
	"O_Soldier_AT_F",
	"O_Soldier_LAT_F",
	"B_soldier_AT_F",
	"B_soldier_LAT_F",
	"B_G_Soldier_LAT_F",
	"O_soldierU_AT_F",
	"O_soldierU_LAT_F",
	"O_recon_LAT_F",
	"B_CTRG_soldier_GL_LAT_F",
	"B_recon_LAT_F"
	];
	
RHQ_AAinf_A3 = 
	[
	"I_Soldier_AA_F",
	"B_soldier_AA_F",
	"O_Soldier_AA_F",
	"O_APC_Tracked_02_AA_F",
	"B_APC_Tracked_01_AA_F",
	"O_soldierU_AA_F"
	];
	
RHQ_Inf_A3 = 
	[
	"I_crew_F",
	"I_engineer_F",
	"I_helicrew_F",
	"I_helipilot_F",
	"I_medic_F",
	"I_officer_F",
	"I_pilot_F",
	"I_Soldier_A_F",
	"I_Soldier_AA_F",
	"I_Soldier_AR_F",
	"I_Soldier_AT_F",
	"I_Soldier_exp_F",
	"I_soldier_F",
	"I_Soldier_GL_F",
	"I_Soldier_LAT_F",
	"I_Soldier_lite_F",
	"I_Soldier_M_F",
	"I_Soldier_repair_F",
	"I_Soldier_SL_F",
	"I_Soldier_TL_F",
	"I_soldier_UAV_F",
	"O_crew_F",
	"O_engineer_F",
	"O_helicrew_F",
	"O_helipilot_F",
	"O_medic_F",
	"O_officer_F",
	"O_Pilot_F",
	"O_Soldier_A_F",
	"O_Soldier_AA_F",
	"O_Soldier_AR_F",
	"O_Soldier_AT_F",
	"O_soldier_exp_F",
	"O_Soldier_F",
	"O_Soldier_GL_F",
	"O_Soldier_LAT_F",
	"O_Soldier_lite_F",
	"O_soldier_M_F",
	"O_soldier_PG_F",
	"O_soldier_repair_F",
	"O_Soldier_SL_F",
	"O_Soldier_TL_F",
	"O_soldier_UAV_F",
	"B_G_engineer_F",
	"B_G_medic_F",
	"B_G_officer_F",
	"B_G_Soldier_A_F",
	"B_G_Soldier_AR_F",
	"B_G_Soldier_exp_F",
	"B_G_Soldier_F",
	"B_G_Soldier_GL_F",
	"B_G_Soldier_LAT_F",
	"B_G_Soldier_lite_F",
	"B_G_Soldier_M_F",
	"B_G_Soldier_SL_F",
	"B_G_Soldier_TL_F",
	"B_crew_F",
	"B_engineer_F",
	"B_helicrew_F",
	"B_Helipilot_F",
	"B_medic_F",
	"B_officer_F",
	"B_Pilot_F",
	"B_Soldier_A_F",
	"B_soldier_AA_F",
	"B_soldier_AR_F",
	"B_soldier_AT_F",
	"B_soldier_exp_F",
	"B_Soldier_F",
	"B_Soldier_GL_F",
	"B_soldier_LAT_F",
	"B_Soldier_lite_F",
	"B_soldier_M_F",
	"B_soldier_PG_F",
	"B_soldier_repair_F",
	"B_Soldier_SL_F",
	"B_Soldier_TL_F",
	"B_soldier_UAV_F",
	"O_recon_exp_F",
	"O_recon_F",
	"O_recon_JTAC_F",
	"O_recon_LAT_F",
	"O_recon_M_F",
	"O_recon_medic_F",
	"O_recon_TL_F",
	"B_recon_exp_F",
	"B_recon_F",
	"B_recon_JTAC_F",
	"B_recon_LAT_F",
	"B_recon_M_F",
	"B_recon_medic_F",
	"B_recon_TL_F",
	"I_Sniper_F",
	"I_Spotter_F",
	"O_sniper_F",
	"O_spotter_F",
	"B_sniper_F",
	"B_spotter_F",
	"O_engineer_U_F",
	"O_soldierU_A_F",
	"O_soldierU_AA_F",
	"O_soldierU_AAA_F",
	"O_soldierU_AAR_F",
	"O_soldierU_AAT_F",
	"O_soldierU_AR_F",
	"O_soldierU_AT_F",
	"O_soldierU_exp_F",
	"O_soldierU_F",
	"O_SoldierU_GL_F",
	"O_soldierU_LAT_F",
	"O_soldierU_M_F",
	"O_soldierU_medic_F",
	"O_soldierU_repair_F",
	"O_SoldierU_SL_F",
	"O_soldierU_TL_F",
	"I_Soldier_AAA_F",
	"I_Soldier_AAR_F",
	"I_Soldier_AAT_F",
	"I_support_AMG_F",
	"I_support_AMort_F",
	"I_support_GMG_F",
	"I_support_MG_F",
	"I_support_Mort_F",
	"O_Soldier_AAA_F",
	"O_Soldier_AAR_F",
	"O_Soldier_AAT_F",
	"O_support_AMG_F",
	"O_support_AMort_F",
	"O_support_GMG_F",
	"O_support_MG_F",
	"O_support_Mort_F",
	"B_soldier_AAA_F",
	"B_soldier_AAR_F",
	"B_soldier_AAT_F",
	"B_support_AMG_F",
	"B_support_AMort_F",
	"B_support_GMG_F",
	"B_support_MG_F",
	"B_support_Mort_F",
	"I_diver_exp_F",
	"I_diver_F",
	"I_diver_TL_F",
	"O_diver_exp_F",
	"O_diver_F",
	"O_diver_TL_F",
	"B_diver_exp_F",
	"B_diver_F",
	"B_diver_TL_F",
	"I_Story_Colonel_F",
	"O_Story_CEO_F",
	"O_Story_Colonel_F",
	"I_G_Story_Protagonist_F",
	"B_Competitor_F",
	"B_RangeMaster_F",
	"B_Story_Colonel_F",
	"B_Story_Engineer_F",
	"B_Story_Pilot_F",
	"B_Story_Protagonist_F",
	"B_Story_SF_Captain_F",
	"B_Story_Tank_Commander_F",
	"B_CTRG_soldier_engineer_exp_F",
	"B_CTRG_soldier_M_medic_F",
	"B_CTRG_soldier_AR_A_F",
	"B_CTRG_soldier_GL_LAT_F",
	"I_G_Story_SF_Captain_F",
	"I_G_resistanceCommander_F",
	"I_G_resistanceLeader_F"
	];
	
RHQ_Art_A3 = 
	[
	"B_MBT_01_arty_F",
	"O_MBT_02_arty_F",
	"B_MBT_01_mlrs_F",
	"I_Mortar_01_F",
	"O_Mortar_01_F",
	"B_G_Mortar_01_F",
	"B_Mortar_01_F"
	];
	
RHQ_HArmor_A3 = 
	[
	"B_MBT_01_cannon_F",
	"B_MBT_01_TUSK_F",
	"O_MBT_02_cannon_F",
	"I_MBT_03_cannon_F"
	];
	
RHQ_MArmor_A3 =
	[

	];
	
RHQ_LArmor_A3 = 
	[
	"I_APC_Wheeled_03_cannon_F",
	"O_APC_Tracked_02_AA_F",
	"O_APC_Tracked_02_cannon_F",
	"O_APC_Wheeled_02_rcws_F",
	"B_APC_Tracked_01_AA_F",
	"B_APC_Tracked_01_rcws_F",
	"B_APC_Wheeled_01_cannon_F",
	"I_APC_tracked_03_cannon_F"
	];
	
RHQ_LArmorAT_A3 = 
	[
	"B_APC_Wheeled_01_cannon_F",
	"I_APC_Wheeled_03_cannon_F",
	"O_APC_Tracked_02_cannon_F",
	"I_APC_tracked_03_cannon_F"
	];
	
RHQ_Cars_A3 = 
	[
	"I_MRAP_03_F",
	"I_MRAP_03_gmg_F",
	"I_MRAP_03_hmg_F",
	"I_Quadbike_01_F",
	"I_Truck_02_covered_F",
	"I_Truck_02_transport_F",
	"O_MRAP_02_F",
	"O_MRAP_02_gmg_F",
	"O_MRAP_02_hmg_F",
	"O_Quadbike_01_F",
	"O_Truck_02_covered_F",
	"O_Truck_02_transport_F",
	"O_Truck_03_device_F",
	"O_Truck_03_transport_F",
	"O_Truck_03_covered_F",
	"B_G_Offroad_01_armed_F",
	"B_G_Offroad_01_F",
	"B_G_Quadbike_01_F",
	"B_G_Van_01_transport_F",
	"B_MRAP_01_F",
	"B_MRAP_01_gmg_F",
	"B_MRAP_01_hmg_F",
	"B_Quadbike_01_F",
	"B_Truck_01_box_F",
	"B_Truck_01_covered_F",
	"B_Truck_01_mover_F",
	"B_Truck_01_transport_F",
	"I_Truck_02_ammo_F",
	"I_Truck_02_box_F",
	"I_Truck_02_fuel_F",
	"I_Truck_02_medical_F",
	"O_Truck_02_Ammo_F",
	"O_Truck_02_box_F",
	"O_Truck_02_fuel_F",
	"O_Truck_02_medical_F",
	"B_G_Van_01_fuel_F",
	"B_Truck_01_ammo_F",
	"B_Truck_01_Repair_F",
	"B_Truck_01_fuel_F",
	"B_Truck_01_medical_F",
	"O_Truck_03_ammo_F",
	"O_Truck_03_fuel_F",
	"O_Truck_03_medical_F",
	"O_Truck_03_repair_F",
	"I_UGV_01_F",
	"I_UGV_01_rcws_F",
	"O_UGV_01_F",
	"O_UGV_01_rcws_F",
	"B_UGV_01_F",
	"B_UGV_01_rcws_F"
	];
	
RHQ_Air_A3 = 
	[
	"I_Heli_Transport_02_F",
	"I_Plane_Fighter_03_AA_F",
	"I_Plane_Fighter_03_CAS_F",
	"B_Plane_CAS_01_F",
	"O_Plane_CAS_02_F",
	"O_Heli_Attack_02_black_F",
	"O_Heli_Attack_02_F",
	"O_Heli_Light_02_F",
	"O_Heli_Light_02_unarmed_F",
	"B_Heli_Attack_01_F",
	"B_Heli_Light_01_armed_F",
	"B_Heli_Light_01_F",
	"B_Heli_Transport_01_camo_F",
	"B_Heli_Transport_01_F",
	"I_UAV_AI",
	"O_UAV_AI",
	"B_UAV_AI",
	"I_UAV_01_F",
	"I_UAV_02_CAS_F",
	"I_UAV_02_F",
	"O_UAV_01_F",
	"O_UAV_02_CAS_F",
	"O_UAV_02_F",
	"B_UAV_01_F",
	"B_UAV_02_CAS_F",
	"B_UAV_02_F",
	"I_Heli_light_03_F",
	"I_Heli_light_03_unarmed_F"
	];
	
RHQ_BAir_A3 = 
	[
	"I_Plane_Fighter_03_CAS_F",
	"B_Plane_CAS_01_F",
	"O_Plane_CAS_02_F"
	];
	
RHQ_RAir_A3 =
	[
	"I_UAV_01_F",
	"I_UAV_02_CAS_F",
	"I_UAV_02_F",
	"O_UAV_01_F",
	"O_UAV_02_CAS_F",
	"O_UAV_02_F",
	"B_UAV_01_F",
	"B_UAV_02_CAS_F",
	"B_UAV_02_F"
	];
	
RHQ_NCAir_A3 = 
	[
	"I_Heli_Transport_02_F",
	"O_Heli_Light_02_unarmed_F",
	"B_Heli_Light_01_F",
	"B_Heli_Transport_01_camo_F",
	"B_Heli_Transport_01_F",
	"I_Heli_light_03_unarmed_F",
	"I_UAV_01_F",
	"I_UAV_02_F",
	"O_UAV_01_F",
	"O_UAV_02_F",
	"B_UAV_01_F",
	"B_UAV_02_F"
	];
	
RHQ_Naval_A3 = 
	[
	"I_Boat_Armed_01_minigun_F",
	"I_Boat_Transport_01_F",
	"O_Boat_Armed_01_hmg_F",
	"O_Boat_Transport_01_F",
	"O_Lifeboat",
	"B_G_Boat_Transport_01_F",
	"B_Boat_Armed_01_minigun_F",
	"B_Boat_Transport_01_F",
	"B_Lifeboat",
	"I_SDV_01_F",
	"O_SDV_01_F",
	"B_SDV_01_F"
	];
	
RHQ_Static_A3 = 
	[
	"I_GMG_01_A_F",
	"I_GMG_01_F",
	"I_GMG_01_high_F",
	"I_HMG_01_A_F",
	"I_HMG_01_F",
	"I_HMG_01_high_F",
	"I_Mortar_01_F",
	"I_static_AA_F",
	"I_static_AT_F",
	"O_GMG_01_A_F",
	"O_GMG_01_F",
	"O_GMG_01_high_F",
	"O_HMG_01_A_F",
	"O_HMG_01_F",
	"O_HMG_01_high_F",
	"O_Mortar_01_F",
	"O_static_AA_F",
	"O_static_AT_F",
	"B_G_Mortar_01_F",
	"B_GMG_01_A_F",
	"B_GMG_01_F",
	"B_GMG_01_high_F",
	"B_HMG_01_A_F",
	"B_HMG_01_F",
	"B_HMG_01_high_F",
	"B_Mortar_01_F",
	"B_static_AA_F",
	"B_static_AT_F"
	];
	
RHQ_StaticAA_A3 = 
	[
	"I_static_AA_F",
	"O_static_AA_F",
	"B_static_AA_F"
	];
	
RHQ_StaticAT_A3 = 
	[
	"I_static_AT_F",
	"O_static_AT_F",
	"B_static_AT_F"
	];
	
RHQ_Support_A3 =
	[
	"I_Truck_02_ammo_F",
	"I_Truck_02_box_F",
	"I_Truck_02_fuel_F",
	"I_Truck_02_medical_F",
	"O_Truck_02_Ammo_F",
	"O_Truck_02_box_F",
	"O_Truck_02_fuel_F",
	"O_Truck_02_medical_F",
	"O_Truck_03_ammo_F",
	"O_Truck_03_fuel_F",
	"O_Truck_03_medical_F",
	"O_Truck_03_repair_F",
	"B_G_Van_01_fuel_F",
	"B_APC_Tracked_01_CRV_F",
	"B_Truck_01_ammo_F",
	"B_Truck_01_Repair_F",
	"B_Truck_01_fuel_F",
	"B_Truck_01_medical_F"
	];
	
RHQ_Cargo_A3 = 
	[
	"I_Heli_Transport_02_F",
	"O_Heli_Attack_02_black_F",
	"O_Heli_Attack_02_F",
	"O_Heli_Light_02_F",
	"O_Heli_Light_02_unarmed_F",
	"B_Heli_Light_01_F",
	"B_Heli_Transport_01_camo_F",
	"B_Heli_Transport_01_F",
	"I_Truck_02_medical_F",
	"O_Truck_02_medical_F",
	"B_Truck_01_medical_F",
	"O_Truck_03_medical_F",
	"I_Boat_Armed_01_minigun_F",
	"I_Boat_Transport_01_F",
	"O_Boat_Armed_01_hmg_F",
	"O_Boat_Transport_01_F",
	"O_Lifeboat",
	"B_G_Boat_Transport_01_F",
	"B_Boat_Armed_01_minigun_F",
	"B_Boat_Transport_01_F",
	"B_Lifeboat",
	"I_SDV_01_F",
	"O_SDV_01_F",
	"B_SDV_01_F",
	"B_MBT_01_cannon_F",
	"I_APC_Wheeled_03_cannon_F",
	"O_APC_Tracked_02_cannon_F",
	"O_APC_Wheeled_02_rcws_F",
	"B_APC_Tracked_01_rcws_F",
	"B_APC_Wheeled_01_cannon_F",
	"I_MRAP_03_F",
	"I_MRAP_03_gmg_F",
	"I_MRAP_03_hmg_F",
	"I_Quadbike_01_F",
	"I_Truck_02_covered_F",
	"I_Truck_02_transport_F",
	"O_MRAP_02_F",
	"O_MRAP_02_gmg_F",
	"O_MRAP_02_hmg_F",
	"O_Quadbike_01_F",
	"O_Truck_02_covered_F",
	"O_Truck_02_transport_F",
	"O_Truck_03_device_F",
	"O_Truck_03_transport_F",
	"O_Truck_03_covered_F",
	"B_G_Offroad_01_armed_F",
	"B_G_Offroad_01_F",
	"B_G_Quadbike_01_F",
	"B_G_Van_01_transport_F",
	"B_MRAP_01_F",
	"B_MRAP_01_gmg_F",
	"B_MRAP_01_hmg_F",
	"B_Quadbike_01_F",
	"B_Truck_01_box_F",
	"B_Truck_01_covered_F",
	"B_Truck_01_mover_F",
	"B_Truck_01_transport_F",
	"I_Heli_light_03_F",
	"I_Heli_light_03_unarmed_F",
	"I_APC_tracked_03_cannon_F"
	];
	
RHQ_NCCargo_A3 = 
	[
	"I_Heli_Transport_02_F",
	"O_Heli_Light_02_unarmed_F",
	"B_Heli_Light_01_F",
	"B_Heli_Transport_01_camo_F",
	"B_Heli_Transport_01_F",
	"I_Heli_light_03_unarmed_F",
	"I_Truck_02_medical_F",
	"O_Truck_02_medical_F",
	"B_Truck_01_medical_F",
	"O_Truck_03_medical_F",
	"I_Boat_Transport_01_F",
	"O_Boat_Transport_01_F",
	"O_Lifeboat",
	"B_G_Boat_Transport_01_F",
	"B_Boat_Transport_01_F",
	"B_Lifeboat",
	"I_MRAP_03_F",
	"I_Quadbike_01_F",
	"I_Truck_02_covered_F",
	"I_Truck_02_transport_F",
	"O_MRAP_02_F",
	"O_Quadbike_01_F",
	"O_Truck_02_covered_F",
	"O_Truck_02_transport_F",
	"O_Truck_03_device_F",
	"O_Truck_03_transport_F",
	"O_Truck_03_covered_F",
	"B_G_Offroad_01_F",
	"B_G_Quadbike_01_F",
	"B_G_Van_01_transport_F",
	"B_MRAP_01_F",
	"B_Quadbike_01_F",
	"B_Truck_01_box_F",
	"B_Truck_01_covered_F",
	"B_Truck_01_mover_F",
	"B_Truck_01_transport_F"
	];
	
RHQ_Crew_A3 = 
	[
	"I_crew_F",
	"I_helicrew_F",
	"I_helipilot_F",
	"I_pilot_F",
	"O_crew_F",
	"O_helicrew_F",
	"O_helipilot_F",
	"O_Pilot_F",
	"B_crew_F",
	"B_helicrew_F",
	"B_Helipilot_F",
	"B_Pilot_F",
	"B_Story_Pilot_F"
	];

RHQ_Other_A3 =
	[
	"I_UAV_AI",
	"O_UAV_AI",
	"B_UAV_AI"
	];

RHQ_Ammo_A3 = 
	[
	"O_Truck_03_ammo_F",
	"I_Truck_02_ammo_F",
	"O_Truck_02_Ammo_F",
	"B_APC_Tracked_01_CRV_F",
	"B_Truck_01_ammo_F"
	];
		
RHQ_Fuel_A3 = 
	[
	"O_Truck_03_fuel_F",
	"I_Truck_02_fuel_F",
	"O_Truck_02_fuel_F",
	"B_G_Van_01_fuel_F",
	"B_APC_Tracked_01_CRV_F",
	"B_Truck_01_fuel_F"	
	];
		
RHQ_Med_A3 = 
	[
	"O_Truck_03_medical_F",
	"I_Truck_02_medical_F",
	"O_Truck_02_medical_F",
	"B_Truck_01_medical_F"
	];
		
RHQ_Rep_A3 =
	[
	"O_Truck_03_repair_F",
	"I_Truck_02_box_F",
	"O_Truck_02_box_F",
	"B_APC_Tracked_01_CRV_F",
	"B_Truck_01_Repair_F"
	];
